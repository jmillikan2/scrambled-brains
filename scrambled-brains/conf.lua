function love.conf(t)
    t.title = "Scrambled Brains"
    t.author = "jesse.a.millikan@gmail.com"
    t.version = "0.8.0"
    t.screen.width = 400
    t.screen.height = 400
end